package com.vgdragon.javadance.Configuration.Extracter;

import java.io.*;
import java.util.zip.GZIPInputStream;

public class ExtractGzip {

    public String extractGzip(String filePathString){
        File file = new File(filePathString);
        String outName = "";
        String fileString = "";

        if(file.exists()){

            try {
                FileInputStream inputStream = new FileInputStream(filePathString);
                FileOutputStream outputStream = null;
                try {
                    GZIPInputStream gzipInputStream = new GZIPInputStream(inputStream);

                    byte[] buffer = new byte[2048];

                    outName = filePathString.substring(0, filePathString.length() - 3);
                    outputStream = new FileOutputStream(outName);

                    int len;
                    String tempString = "";

                    InputStreamReader inputStreamReader = new InputStreamReader(gzipInputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    while ((tempString = bufferedReader.readLine()) != null){
                        fileString += tempString;
                    }



                } finally {
                    if(outputStream != null){
                        outputStream.close();

                    }
                    inputStream.close();
                }

            } catch (IOException e) {
                e.printStackTrace();

            }


        }
        return fileString;
    }


}
