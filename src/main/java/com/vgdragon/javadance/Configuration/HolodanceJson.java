package com.vgdragon.javadance.Configuration;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.vgdragon.javadance.Configuration.Extracter.ExtractGzip;
import com.vgdragon.javadance.Configuration.JsonClasses.*;
import com.vgdragon.javadance.Configuration.JsonClasses.GlobalClasses.PlaySession;

import java.io.File;
import java.io.IOException;

public class HolodanceJson {

    public ConfigurationJson getConfigurationJson(String holodanceFolder) throws IOException {
         return new ObjectMapper()
                 .readValue(
                         new File(pathCleaner(holodanceFolder) + "Configuration.json"),
                         ConfigurationJson.class);
    }

    public PlaySessionsJson getPlaySessionsJson(String holodanceFolder) throws IOException {
        ConfigurationJson configurationJson = getConfigurationJson(holodanceFolder);
        return new ObjectMapper()
                .readValue(
                        new File(configurationJson.getPathPlaySessions()),
                        PlaySessionsJson.class);
    }

    public FileSystemCacheOsuJson getFileSystemCacheOsuJson(String holodanceFolder) throws IOException {
        ConfigurationJson configurationJson = getConfigurationJson(holodanceFolder);
        return new ObjectMapper()
                .readValue(
                        new File(configurationJson.getPathOsuJsonFileSystemCache()),
                        FileSystemCacheOsuJson.class);
    }

    public FileSystemCacheOsuExtraJson getFileSystemCacheOsuExtraJson(String holodanceFolder) throws IOException {
        ConfigurationJson configurationJson = getConfigurationJson(holodanceFolder);
        return new ObjectMapper()
                .readValue(
                        new File(configurationJson.getPathOsuJsonFileSystemCacheExtra()),
                        FileSystemCacheOsuExtraJson.class);
    }

    public FileSystemCacheOsuFavoritesJson getFileSystemCacheOsuFavoritesJson(String holodanceFolder) throws IOException {
        ConfigurationJson configurationJson = getConfigurationJson(holodanceFolder);
        return new ObjectMapper()
                .readValue(
                        new File(configurationJson.getPathOsuJsonFavorites()),
                        FileSystemCacheOsuFavoritesJson.class);
    }

    public FileSystemCacheOsuLastPlayedJson getFileSystemCacheOsuLastPlayed(String holodanceFolder) throws IOException {
        ConfigurationJson configurationJson = getConfigurationJson(holodanceFolder);
        return new ObjectMapper()
                .readValue(
                        new File(configurationJson.getPathPlaySessions()),
                        FileSystemCacheOsuLastPlayedJson.class);
    }

    public CurrentScoreJson getCurrentScoreJson(String holodanceFolder) throws IOException {
        ConfigurationJson configurationJson = getConfigurationJson(holodanceFolder);
        return new ObjectMapper()
                .readValue(
                        new File(configurationJson.getPathCurrentScoreFile()),
                        CurrentScoreJson.class);
    }

    public ReplayJson getReplayJson(String holodanceFolder, PlaySession playSession) throws IOException {

        String replayFolderParthString = getConfigurationJson(holodanceFolder).getPathPlaySessionsBaseFolder();

        if (playSession.getGameType() == 0) {
            replayFolderParthString += "Holodance\\";

        } else if (playSession.getGameType() == 1) {
            replayFolderParthString += "HolodanceCustom\\";

        } else if (playSession.getGameType() == 2) {
            replayFolderParthString += "osu\\";

        } else if (playSession.getGameType() == 3) {
            replayFolderParthString += "StepMania\\";
        }
        replayFolderParthString += playSession.getLevelId() + "\\" + playSession.getTrackId() + "\\" + playSession.getPlayedDate() + ".json.gz";

        return new ObjectMapper().readValue(new ExtractGzip().extractGzip(replayFolderParthString), ReplayJson.class);
    }

    public ReplayJson getReplayJson (File file) throws IOException {
        return new ObjectMapper().readValue(file, ReplayJson.class);
    }

    private String pathCleaner(String path){
        if(path.endsWith("\\") || path.endsWith("/")){
            return path;
        } else {
            return path + "\\";
        }
    }


}
