package com.vgdragon.javadance.Configuration.JsonClasses.ReplayClasses;

public class Step {

    private double time;
    private Position position;
    private Rotation rotation;

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Rotation getRotation() {
        return rotation;
    }

    public void setRotation(Rotation rotation) {
        this.rotation = rotation;
    }
}
