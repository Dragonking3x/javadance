package com.vgdragon.javadance.Configuration.JsonClasses.ReplayClasses;

public class RecordedEvent {

    private int eventIndex;
    private int touchType;
    private int pickupType;
    private Position platformPositionAtInstantiation;
    private Rotation platformRotationAtInstantiation;
    private double instantiateTime;
    private Position instantiatePosition;
    private double perfectCatchTime;
    private Position perfectCatchPosition;
    private double noteDuration;
    private double flyDuration;
    private double showTimingDuration;
    private int repeatCount;
    private int baseScore;
    private boolean midiVelocityOrb;
    private int midiVelocityAddScore;
    private double normalizedMidiVelocity;
    private int baseScoreForMidiVelocityOrb;
    private int bonusOrbVelocity;
    private int bonusOrbRotation;
    private int bonusHead;
    private int bonusFeet;
    private double headOfftimeMultiplier;
    private int bonusOrbDistance;
    private int bonusSlider;
    private int bonusSpinner;
    private boolean caught;
    private double timingOffset;
    private double timingOffsetWithHeadTolerance;
    private int caughtWith;
    private int caughtWithDifferent;
    private int caughtWithDifferentBonus;
    private double catchTime;
    private Position catchPosition;
    private int comboCount;
    private int currentMultiplier;
    private int rawScoreWithBonus;
    private int timedScoreWithBonus;
    private int actualBonusHead;
    private int actualBonusFeet;
    private double velocityBonusFactor;
    private int actualVelocityBonusScore;
    private double rotationBonusFactor;
    private int actualRotationBonusScore;
    private double distance;
    private double distanceMultiplier;
    private double distanceByTimeMultiplier;
    private int actualBonusOrbDistance;
    private int actualBonusSlider;
    private int actualBonusSpinner;
    private double accumulatedTime;
    private double scoreWithMultiplier;
    private double balanceMultiplier;
    private int finalScore;
    private int totalScore;

    public int getEventIndex() {
        return eventIndex;
    }

    public void setEventIndex(int eventIndex) {
        this.eventIndex = eventIndex;
    }

    public int getTouchType() {
        return touchType;
    }

    public void setTouchType(int touchType) {
        this.touchType = touchType;
    }

    public int getPickupType() {
        return pickupType;
    }

    public void setPickupType(int pickupType) {
        this.pickupType = pickupType;
    }

    public Position getPlatformPositionAtInstantiation() {
        return platformPositionAtInstantiation;
    }

    public void setPlatformPositionAtInstantiation(Position platformPositionAtInstantiation) {
        this.platformPositionAtInstantiation = platformPositionAtInstantiation;
    }

    public Rotation getPlatformRotationAtInstantiation() {
        return platformRotationAtInstantiation;
    }

    public void setPlatformRotationAtInstantiation(Rotation platformRotationAtInstantiation) {
        this.platformRotationAtInstantiation = platformRotationAtInstantiation;
    }

    public double getInstantiateTime() {
        return instantiateTime;
    }

    public void setInstantiateTime(double instantiateTime) {
        this.instantiateTime = instantiateTime;
    }

    public Position getInstantiatePosition() {
        return instantiatePosition;
    }

    public void setInstantiatePosition(Position instantiatePosition) {
        this.instantiatePosition = instantiatePosition;
    }

    public double getPerfectCatchTime() {
        return perfectCatchTime;
    }

    public void setPerfectCatchTime(double perfectCatchTime) {
        this.perfectCatchTime = perfectCatchTime;
    }

    public Position getPerfectCatchPosition() {
        return perfectCatchPosition;
    }

    public void setPerfectCatchPosition(Position perfectCatchPosition) {
        this.perfectCatchPosition = perfectCatchPosition;
    }

    public double getNoteDuration() {
        return noteDuration;
    }

    public void setNoteDuration(double noteDuration) {
        this.noteDuration = noteDuration;
    }

    public double getFlyDuration() {
        return flyDuration;
    }

    public void setFlyDuration(double flyDuration) {
        this.flyDuration = flyDuration;
    }

    public double getShowTimingDuration() {
        return showTimingDuration;
    }

    public void setShowTimingDuration(double showTimingDuration) {
        this.showTimingDuration = showTimingDuration;
    }

    public int getRepeatCount() {
        return repeatCount;
    }

    public void setRepeatCount(int repeatCount) {
        this.repeatCount = repeatCount;
    }

    public int getBaseScore() {
        return baseScore;
    }

    public void setBaseScore(int baseScore) {
        this.baseScore = baseScore;
    }

    public boolean isMidiVelocityOrb() {
        return midiVelocityOrb;
    }

    public void setMidiVelocityOrb(boolean midiVelocityOrb) {
        this.midiVelocityOrb = midiVelocityOrb;
    }

    public int getMidiVelocityAddScore() {
        return midiVelocityAddScore;
    }

    public void setMidiVelocityAddScore(int midiVelocityAddScore) {
        this.midiVelocityAddScore = midiVelocityAddScore;
    }

    public double getNormalizedMidiVelocity() {
        return normalizedMidiVelocity;
    }

    public void setNormalizedMidiVelocity(double normalizedMidiVelocity) {
        this.normalizedMidiVelocity = normalizedMidiVelocity;
    }

    public int getBaseScoreForMidiVelocityOrb() {
        return baseScoreForMidiVelocityOrb;
    }

    public void setBaseScoreForMidiVelocityOrb(int baseScoreForMidiVelocityOrb) {
        this.baseScoreForMidiVelocityOrb = baseScoreForMidiVelocityOrb;
    }

    public int getBonusOrbVelocity() {
        return bonusOrbVelocity;
    }

    public void setBonusOrbVelocity(int bonusOrbVelocity) {
        this.bonusOrbVelocity = bonusOrbVelocity;
    }

    public int getBonusOrbRotation() {
        return bonusOrbRotation;
    }

    public void setBonusOrbRotation(int bonusOrbRotation) {
        this.bonusOrbRotation = bonusOrbRotation;
    }

    public int getBonusHead() {
        return bonusHead;
    }

    public void setBonusHead(int bonusHead) {
        this.bonusHead = bonusHead;
    }

    public int getBonusFeet() {
        return bonusFeet;
    }

    public void setBonusFeet(int bonusFeet) {
        this.bonusFeet = bonusFeet;
    }

    public double getHeadOfftimeMultiplier() {
        return headOfftimeMultiplier;
    }

    public void setHeadOfftimeMultiplier(double headOfftimeMultiplier) {
        this.headOfftimeMultiplier = headOfftimeMultiplier;
    }

    public int getBonusOrbDistance() {
        return bonusOrbDistance;
    }

    public void setBonusOrbDistance(int bonusOrbDistance) {
        this.bonusOrbDistance = bonusOrbDistance;
    }

    public int getBonusSlider() {
        return bonusSlider;
    }

    public void setBonusSlider(int bonusSlider) {
        this.bonusSlider = bonusSlider;
    }

    public int getBonusSpinner() {
        return bonusSpinner;
    }

    public void setBonusSpinner(int bonusSpinner) {
        this.bonusSpinner = bonusSpinner;
    }

    public boolean isCaught() {
        return caught;
    }

    public void setCaught(boolean caught) {
        this.caught = caught;
    }

    public double getTimingOffset() {
        return timingOffset;
    }

    public void setTimingOffset(double timingOffset) {
        this.timingOffset = timingOffset;
    }

    public double getTimingOffsetWithHeadTolerance() {
        return timingOffsetWithHeadTolerance;
    }

    public void setTimingOffsetWithHeadTolerance(double timingOffsetWithHeadTolerance) {
        this.timingOffsetWithHeadTolerance = timingOffsetWithHeadTolerance;
    }

    public int getCaughtWith() {
        return caughtWith;
    }

    public void setCaughtWith(int caughtWith) {
        this.caughtWith = caughtWith;
    }

    public int getCaughtWithDifferent() {
        return caughtWithDifferent;
    }

    public void setCaughtWithDifferent(int caughtWithDifferent) {
        this.caughtWithDifferent = caughtWithDifferent;
    }

    public int getCaughtWithDifferentBonus() {
        return caughtWithDifferentBonus;
    }

    public void setCaughtWithDifferentBonus(int caughtWithDifferentBonus) {
        this.caughtWithDifferentBonus = caughtWithDifferentBonus;
    }

    public double getCatchTime() {
        return catchTime;
    }

    public void setCatchTime(double catchTime) {
        this.catchTime = catchTime;
    }

    public Position getCatchPosition() {
        return catchPosition;
    }

    public void setCatchPosition(Position catchPosition) {
        this.catchPosition = catchPosition;
    }

    public int getComboCount() {
        return comboCount;
    }

    public void setComboCount(int comboCount) {
        this.comboCount = comboCount;
    }

    public int getCurrentMultiplier() {
        return currentMultiplier;
    }

    public void setCurrentMultiplier(int currentMultiplier) {
        this.currentMultiplier = currentMultiplier;
    }

    public int getRawScoreWithBonus() {
        return rawScoreWithBonus;
    }

    public void setRawScoreWithBonus(int rawScoreWithBonus) {
        this.rawScoreWithBonus = rawScoreWithBonus;
    }

    public int getTimedScoreWithBonus() {
        return timedScoreWithBonus;
    }

    public void setTimedScoreWithBonus(int timedScoreWithBonus) {
        this.timedScoreWithBonus = timedScoreWithBonus;
    }

    public int getActualBonusHead() {
        return actualBonusHead;
    }

    public void setActualBonusHead(int actualBonusHead) {
        this.actualBonusHead = actualBonusHead;
    }

    public int getActualBonusFeet() {
        return actualBonusFeet;
    }

    public void setActualBonusFeet(int actualBonusFeet) {
        this.actualBonusFeet = actualBonusFeet;
    }

    public double getVelocityBonusFactor() {
        return velocityBonusFactor;
    }

    public void setVelocityBonusFactor(double velocityBonusFactor) {
        this.velocityBonusFactor = velocityBonusFactor;
    }

    public int getActualVelocityBonusScore() {
        return actualVelocityBonusScore;
    }

    public void setActualVelocityBonusScore(int actualVelocityBonusScore) {
        this.actualVelocityBonusScore = actualVelocityBonusScore;
    }

    public double getRotationBonusFactor() {
        return rotationBonusFactor;
    }

    public void setRotationBonusFactor(double rotationBonusFactor) {
        this.rotationBonusFactor = rotationBonusFactor;
    }

    public int getActualRotationBonusScore() {
        return actualRotationBonusScore;
    }

    public void setActualRotationBonusScore(int actualRotationBonusScore) {
        this.actualRotationBonusScore = actualRotationBonusScore;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getDistanceMultiplier() {
        return distanceMultiplier;
    }

    public void setDistanceMultiplier(double distanceMultiplier) {
        this.distanceMultiplier = distanceMultiplier;
    }

    public double getDistanceByTimeMultiplier() {
        return distanceByTimeMultiplier;
    }

    public void setDistanceByTimeMultiplier(double distanceByTimeMultiplier) {
        this.distanceByTimeMultiplier = distanceByTimeMultiplier;
    }

    public int getActualBonusOrbDistance() {
        return actualBonusOrbDistance;
    }

    public void setActualBonusOrbDistance(int actualBonusOrbDistance) {
        this.actualBonusOrbDistance = actualBonusOrbDistance;
    }

    public int getActualBonusSlider() {
        return actualBonusSlider;
    }

    public void setActualBonusSlider(int actualBonusSlider) {
        this.actualBonusSlider = actualBonusSlider;
    }

    public int getActualBonusSpinner() {
        return actualBonusSpinner;
    }

    public void setActualBonusSpinner(int actualBonusSpinner) {
        this.actualBonusSpinner = actualBonusSpinner;
    }

    public double getAccumulatedTime() {
        return accumulatedTime;
    }

    public void setAccumulatedTime(double accumulatedTime) {
        this.accumulatedTime = accumulatedTime;
    }

    public double getScoreWithMultiplier() {
        return scoreWithMultiplier;
    }

    public void setScoreWithMultiplier(double scoreWithMultiplier) {
        this.scoreWithMultiplier = scoreWithMultiplier;
    }

    public double getBalanceMultiplier() {
        return balanceMultiplier;
    }

    public void setBalanceMultiplier(double balanceMultiplier) {
        this.balanceMultiplier = balanceMultiplier;
    }

    public int getFinalScore() {
        return finalScore;
    }

    public void setFinalScore(int finalScore) {
        this.finalScore = finalScore;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }
}
