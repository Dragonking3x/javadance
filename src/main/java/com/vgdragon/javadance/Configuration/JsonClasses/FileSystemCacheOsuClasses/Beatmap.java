package com.vgdragon.javadance.Configuration.JsonClasses.FileSystemCacheOsuClasses;

import com.vgdragon.javadance.Configuration.JsonClasses.GlobalClasses.PlaySession;

import java.util.List;

public class Beatmap {


    private long lastModified;
    private long lastPlayed;
    private int lastLoadedBeatmapFile;
    private boolean beatmapFileCausedError;
    private boolean audioFileCausedError;
    private int startedCount;
    private int finishedCount;
    private int score;
    private int maxScore;
    private List<PlaySession> playSessions;

    private String title;
    private String titleUnicode;
    private String titleForSort;
    private String artist;
    private String artistUnicode;
    private String artistForSort;
    private String audioFile;
    private int bpm;
    private int bpmChanges;
    private int bpmMin;
    private int bpmMax;
    private double audioPreviewTime;
    private double durationSeconds;
    private int beatmapSetID;
    private int beatmapID;
    private String beatmapHash;
    private double difficulty;
    private String version;
    private double approachRate;
    private int orbCount;
    private int sliderCount;
    private int spinnerCount;
    private int mapGapsGreater10Seconds;
    private int mapGapsGreater30Seconds;
    private double maxTimeBetweenEvents;
    private String creator;
    private String fileName;
    private int gameMode;

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public long getLastPlayed() {
        return lastPlayed;
    }

    public void setLastPlayed(long lastPlayed) {
        this.lastPlayed = lastPlayed;
    }

    public int getLastLoadedBeatmapFile() {
        return lastLoadedBeatmapFile;
    }

    public void setLastLoadedBeatmapFile(int lastLoadedBeatmapFile) {
        this.lastLoadedBeatmapFile = lastLoadedBeatmapFile;
    }

    public boolean isBeatmapFileCausedError() {
        return beatmapFileCausedError;
    }

    public void setBeatmapFileCausedError(boolean beatmapFileCausedError) {
        this.beatmapFileCausedError = beatmapFileCausedError;
    }

    public boolean isAudioFileCausedError() {
        return audioFileCausedError;
    }

    public void setAudioFileCausedError(boolean audioFileCausedError) {
        this.audioFileCausedError = audioFileCausedError;
    }

    public int getStartedCount() {
        return startedCount;
    }

    public void setStartedCount(int startedCount) {
        this.startedCount = startedCount;
    }

    public int getFinishedCount() {
        return finishedCount;
    }

    public void setFinishedCount(int finishedCount) {
        this.finishedCount = finishedCount;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(int maxScore) {
        this.maxScore = maxScore;
    }

    public List<PlaySession> getPlaySessions() {
        return playSessions;
    }

    public void setPlaySessions(List<PlaySession> playSessions) {
        this.playSessions = playSessions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleUnicode() {
        return titleUnicode;
    }

    public void setTitleUnicode(String titleUnicode) {
        this.titleUnicode = titleUnicode;
    }

    public String getTitleForSort() {
        return titleForSort;
    }

    public void setTitleForSort(String titleForSort) {
        this.titleForSort = titleForSort;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getArtistUnicode() {
        return artistUnicode;
    }

    public void setArtistUnicode(String artistUnicode) {
        this.artistUnicode = artistUnicode;
    }

    public String getArtistForSort() {
        return artistForSort;
    }

    public void setArtistForSort(String artistForSort) {
        this.artistForSort = artistForSort;
    }

    public String getAudioFile() {
        return audioFile;
    }

    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }

    public int getBpm() {
        return bpm;
    }

    public void setBpm(int bpm) {
        this.bpm = bpm;
    }

    public int getBpmChanges() {
        return bpmChanges;
    }

    public void setBpmChanges(int bpmChanges) {
        this.bpmChanges = bpmChanges;
    }

    public int getBpmMin() {
        return bpmMin;
    }

    public void setBpmMin(int bpmMin) {
        this.bpmMin = bpmMin;
    }

    public int getBpmMax() {
        return bpmMax;
    }

    public void setBpmMax(int bpmMax) {
        this.bpmMax = bpmMax;
    }

    public double getAudioPreviewTime() {
        return audioPreviewTime;
    }

    public void setAudioPreviewTime(double audioPreviewTime) {
        this.audioPreviewTime = audioPreviewTime;
    }

    public double getDurationSeconds() {
        return durationSeconds;
    }

    public void setDurationSeconds(double durationSeconds) {
        this.durationSeconds = durationSeconds;
    }

    public int getBeatmapSetID() {
        return beatmapSetID;
    }

    public void setBeatmapSetID(int beatmapSetID) {
        this.beatmapSetID = beatmapSetID;
    }

    public int getBeatmapID() {
        return beatmapID;
    }

    public void setBeatmapID(int beatmapID) {
        this.beatmapID = beatmapID;
    }

    public String getBeatmapHash() {
        return beatmapHash;
    }

    public void setBeatmapHash(String beatmapHash) {
        this.beatmapHash = beatmapHash;
    }

    public double getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(double difficulty) {
        this.difficulty = difficulty;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public double getApproachRate() {
        return approachRate;
    }

    public void setApproachRate(double approachRate) {
        this.approachRate = approachRate;
    }

    public int getOrbCount() {
        return orbCount;
    }

    public void setOrbCount(int orbCount) {
        this.orbCount = orbCount;
    }

    public int getSliderCount() {
        return sliderCount;
    }

    public void setSliderCount(int sliderCount) {
        this.sliderCount = sliderCount;
    }

    public int getSpinnerCount() {
        return spinnerCount;
    }

    public void setSpinnerCount(int spinnerCount) {
        this.spinnerCount = spinnerCount;
    }

    public int getMapGapsGreater10Seconds() {
        return mapGapsGreater10Seconds;
    }

    public void setMapGapsGreater10Seconds(int mapGapsGreater10Seconds) {
        this.mapGapsGreater10Seconds = mapGapsGreater10Seconds;
    }

    public int getMapGapsGreater30Seconds() {
        return mapGapsGreater30Seconds;
    }

    public void setMapGapsGreater30Seconds(int mapGapsGreater30Seconds) {
        this.mapGapsGreater30Seconds = mapGapsGreater30Seconds;
    }

    public double getMaxTimeBetweenEvents() {
        return maxTimeBetweenEvents;
    }

    public void setMaxTimeBetweenEvents(double maxTimeBetweenEvents) {
        this.maxTimeBetweenEvents = maxTimeBetweenEvents;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getGameMode() {
        return gameMode;
    }

    public void setGameMode(int gameMode) {
        this.gameMode = gameMode;
    }
}
