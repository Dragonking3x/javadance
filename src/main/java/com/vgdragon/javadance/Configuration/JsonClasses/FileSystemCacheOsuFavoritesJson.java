package com.vgdragon.javadance.Configuration.JsonClasses;

import com.vgdragon.javadance.Configuration.JsonClasses.GlobalClasses.Song;

import java.util.List;

public class FileSystemCacheOsuFavoritesJson {

    private int version;
    private long lastEagerSync;
    private List<Song> songs;


    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public long getLastEagerSync() {
        return lastEagerSync;
    }

    public void setLastEagerSync(long lastEagerSync) {
        this.lastEagerSync = lastEagerSync;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }
}
