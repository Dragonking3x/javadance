package com.vgdragon.javadance.Configuration.JsonClasses.GlobalClasses;

import com.vgdragon.javadance.Configuration.JsonClasses.FileSystemCacheOsuClasses.Beatmap;

import java.util.List;

public class Song {

    private long lastModified;
    private long lastPlayed;
    private int startedCount;
    private int finishedCount;
    private int score;
    private int maxScore;
    private String title;
    private String titleUnicode;
    private String titleForSort;
    private String artist;
    private String artistUnicode;
    private String artistForSort;
    private List<String> tags;
    private String audioFile;
    private String imageFile;
    private int bpm;
    private int bpmChanges;
    private int bpmMin;
    private int bpmMax;
    private double audioPreviewTime;
    private double durationSeconds;
    private int beatmapSetID;
    private List<Integer> gameModes;
    private String folderPath;
    private String folderName;
    private List<Beatmap> beatmaps;

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public long getLastPlayed() {
        return lastPlayed;
    }

    public void setLastPlayed(long lastPlayed) {
        this.lastPlayed = lastPlayed;
    }

    public int getStartedCount() {
        return startedCount;
    }

    public void setStartedCount(int startedCount) {
        this.startedCount = startedCount;
    }

    public int getFinishedCount() {
        return finishedCount;
    }

    public void setFinishedCount(int finishedCount) {
        this.finishedCount = finishedCount;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(int maxScore) {
        this.maxScore = maxScore;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleUnicode() {
        return titleUnicode;
    }

    public void setTitleUnicode(String titleUnicode) {
        this.titleUnicode = titleUnicode;
    }

    public String getTitleForSort() {
        return titleForSort;
    }

    public void setTitleForSort(String titleForSort) {
        this.titleForSort = titleForSort;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getArtistUnicode() {
        return artistUnicode;
    }

    public void setArtistUnicode(String artistUnicode) {
        this.artistUnicode = artistUnicode;
    }

    public String getArtistForSort() {
        return artistForSort;
    }

    public void setArtistForSort(String artistForSort) {
        this.artistForSort = artistForSort;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getAudioFile() {
        return audioFile;
    }

    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public int getBpm() {
        return bpm;
    }

    public void setBpm(int bpm) {
        this.bpm = bpm;
    }

    public int getBpmChanges() {
        return bpmChanges;
    }

    public void setBpmChanges(int bpmChanges) {
        this.bpmChanges = bpmChanges;
    }

    public int getBpmMin() {
        return bpmMin;
    }

    public void setBpmMin(int bpmMin) {
        this.bpmMin = bpmMin;
    }

    public int getBpmMax() {
        return bpmMax;
    }

    public void setBpmMax(int bpmMax) {
        this.bpmMax = bpmMax;
    }

    public double getAudioPreviewTime() {
        return audioPreviewTime;
    }

    public void setAudioPreviewTime(double audioPreviewTime) {
        this.audioPreviewTime = audioPreviewTime;
    }

    public double getDurationSeconds() {
        return durationSeconds;
    }

    public void setDurationSeconds(double durationSeconds) {
        this.durationSeconds = durationSeconds;
    }

    public int getBeatmapSetID() {
        return beatmapSetID;
    }

    public void setBeatmapSetID(int beatmapSetID) {
        this.beatmapSetID = beatmapSetID;
    }

    public List<Integer> getGameModes() {
        return gameModes;
    }

    public void setGameModes(List<Integer> gameModes) {
        this.gameModes = gameModes;
    }

    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public List<Beatmap> getBeatmaps() {
        return beatmaps;
    }

    public void setBeatmaps(List<Beatmap> beatmaps) {
        this.beatmaps = beatmaps;
    }
}
