package com.vgdragon.javadance.Configuration.JsonClasses.ReplayClasses;

import com.vgdragon.javadance.Configuration.JsonClasses.GlobalClasses.Player;

import java.util.List;

public class ReplayData {

    private int version;
    private String gameVersion;
    private long playedDate;

    private Player playerSelf;

    private List<RecordedEvent> recordedEvents;

    private List<Step> leftHandSteps;
    private List<Step> rightHandSteps;
    private List<Step> headSteps;
    private List<Step> movePlayerAreaSteps;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getGameVersion() {
        return gameVersion;
    }

    public void setGameVersion(String gameVersion) {
        this.gameVersion = gameVersion;
    }

    public long getPlayedDate() {
        return playedDate;
    }

    public void setPlayedDate(long playedDate) {
        this.playedDate = playedDate;
    }

    public Player getPlayerSelf() {
        return playerSelf;
    }

    public void setPlayerSelf(Player playerSelf) {
        this.playerSelf = playerSelf;
    }

    public List<RecordedEvent> getRecordedEvents() {
        return recordedEvents;
    }

    public void setRecordedEvents(List<RecordedEvent> recordedEvents) {
        this.recordedEvents = recordedEvents;
    }

    public List<Step> getLeftHandSteps() {
        return leftHandSteps;
    }

    public void setLeftHandSteps(List<Step> leftHandSteps) {
        this.leftHandSteps = leftHandSteps;
    }

    public List<Step> getRightHandSteps() {
        return rightHandSteps;
    }

    public void setRightHandSteps(List<Step> rightHandSteps) {
        this.rightHandSteps = rightHandSteps;
    }

    public List<Step> getHeadSteps() {
        return headSteps;
    }

    public void setHeadSteps(List<Step> headSteps) {
        this.headSteps = headSteps;
    }

    public List<Step> getMovePlayerAreaSteps() {
        return movePlayerAreaSteps;
    }

    public void setMovePlayerAreaSteps(List<Step> movePlayerAreaSteps) {
        this.movePlayerAreaSteps = movePlayerAreaSteps;
    }
}
