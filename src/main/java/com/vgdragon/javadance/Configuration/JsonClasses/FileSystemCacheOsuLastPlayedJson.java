package com.vgdragon.javadance.Configuration.JsonClasses;

import com.vgdragon.javadance.Configuration.JsonClasses.FileSystemCacheOsuClasses.Beatmap;

import java.util.List;

public class FileSystemCacheOsuLastPlayedJson {

    private int version;
    private List<Beatmap> playedBeatmaps;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public List<Beatmap> getPlayedBeatmaps() {
        return playedBeatmaps;
    }

    public void setPlayedBeatmaps(List<Beatmap> playedBeatmaps) {
        this.playedBeatmaps = playedBeatmaps;
    }
}
