package com.vgdragon.javadance.Configuration.JsonClasses.GlobalClasses;

public class Player {

    private int version;
    private int playerPlatforms;
    private long steamID;
    private String nickname;
    private double playerHeight;
    private double playerChestHeight;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getPlayerPlatforms() {
        return playerPlatforms;
    }

    public void setPlayerPlatforms(int playerPlatforms) {
        this.playerPlatforms = playerPlatforms;
    }

    public long getSteamID() {
        return steamID;
    }

    public void setSteamID(long steamID) {
        this.steamID = steamID;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public double getPlayerHeight() {
        return playerHeight;
    }

    public void setPlayerHeight(double playerHeight) {
        this.playerHeight = playerHeight;
    }

    public double getPlayerChestHeight() {
        return playerChestHeight;
    }

    public void setPlayerChestHeight(double playerChestHeight) {
        this.playerChestHeight = playerChestHeight;
    }
}
