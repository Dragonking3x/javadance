package com.vgdragon.javadance.Configuration.JsonClasses;

public class ConfigurationJson {
    
    private int version;
    private String pathPartyModeHighscores;
    private String pathLocalMP3s;
    private boolean useOsuDefaultFolder;
    private boolean useOsuDbForDefaultFolder;
    private String pathOsuSongs;
    private boolean useOsuExtraFolder;
    private boolean autoDownloadOsuBeatmaps;
    private String pathOsuSongsExtra;
    private String pathStepmaniaSongs;
    private String pathPlaySessions;
    private String pathPlaySessionsText;
    private String pathPlaySessionsBaseFolder;
    private String pathOsuJsonFileSystemCache;
    private String pathOsuJsonFileSystemCacheExtra;
    private String pathOsuJsonFavorites;
    private String pathOsuJsonPlaySessions;
    private boolean updateCurrentScoreToFile;
    private boolean forceWriteScoreToFile;
    private int updateCurrentScoreFrequencySeconds;
    private String pathCurrentScoreFile;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getPathPartyModeHighscores() {
        return pathPartyModeHighscores;
    }

    public void setPathPartyModeHighscores(String pathPartyModeHighscores) {
        this.pathPartyModeHighscores = pathPartyModeHighscores;
    }

    public String getPathLocalMP3s() {
        return pathLocalMP3s;
    }

    public void setPathLocalMP3s(String pathLocalMP3s) {
        this.pathLocalMP3s = pathLocalMP3s;
    }

    public boolean isUseOsuDefaultFolder() {
        return useOsuDefaultFolder;
    }

    public void setUseOsuDefaultFolder(boolean useOsuDefaultFolder) {
        this.useOsuDefaultFolder = useOsuDefaultFolder;
    }

    public boolean isUseOsuDbForDefaultFolder() {
        return useOsuDbForDefaultFolder;
    }

    public void setUseOsuDbForDefaultFolder(boolean useOsuDbForDefaultFolder) {
        this.useOsuDbForDefaultFolder = useOsuDbForDefaultFolder;
    }

    public String getPathOsuSongs() {
        return pathOsuSongs;
    }

    public void setPathOsuSongs(String pathOsuSongs) {
        this.pathOsuSongs = pathOsuSongs;
    }

    public boolean isUseOsuExtraFolder() {
        return useOsuExtraFolder;
    }

    public void setUseOsuExtraFolder(boolean useOsuExtraFolder) {
        this.useOsuExtraFolder = useOsuExtraFolder;
    }

    public boolean isAutoDownloadOsuBeatmaps() {
        return autoDownloadOsuBeatmaps;
    }

    public void setAutoDownloadOsuBeatmaps(boolean autoDownloadOsuBeatmaps) {
        this.autoDownloadOsuBeatmaps = autoDownloadOsuBeatmaps;
    }

    public String getPathOsuSongsExtra() {
        return pathOsuSongsExtra;
    }

    public void setPathOsuSongsExtra(String pathOsuSongsExtra) {
        this.pathOsuSongsExtra = pathOsuSongsExtra;
    }

    public String getPathStepmaniaSongs() {
        return pathStepmaniaSongs;
    }

    public void setPathStepmaniaSongs(String pathStepmaniaSongs) {
        this.pathStepmaniaSongs = pathStepmaniaSongs;
    }

    public String getPathPlaySessions() {
        return pathPlaySessions;
    }

    public void setPathPlaySessions(String pathPlaySessions) {
        this.pathPlaySessions = pathPlaySessions;
    }

    public String getPathPlaySessionsText() {
        return pathPlaySessionsText;
    }

    public void setPathPlaySessionsText(String pathPlaySessionsText) {
        this.pathPlaySessionsText = pathPlaySessionsText;
    }

    public String getPathPlaySessionsBaseFolder() {
        return pathPlaySessionsBaseFolder;
    }

    public void setPathPlaySessionsBaseFolder(String pathPlaySessionsBaseFolder) {
        this.pathPlaySessionsBaseFolder = pathPlaySessionsBaseFolder;
    }

    public String getPathOsuJsonFileSystemCache() {
        return pathOsuJsonFileSystemCache;
    }

    public void setPathOsuJsonFileSystemCache(String pathOsuJsonFileSystemCache) {
        this.pathOsuJsonFileSystemCache = pathOsuJsonFileSystemCache;
    }

    public String getPathOsuJsonFileSystemCacheExtra() {
        return pathOsuJsonFileSystemCacheExtra;
    }

    public void setPathOsuJsonFileSystemCacheExtra(String pathOsuJsonFileSystemCacheExtra) {
        this.pathOsuJsonFileSystemCacheExtra = pathOsuJsonFileSystemCacheExtra;
    }

    public String getPathOsuJsonFavorites() {
        return pathOsuJsonFavorites;
    }

    public void setPathOsuJsonFavorites(String pathOsuJsonFavorites) {
        this.pathOsuJsonFavorites = pathOsuJsonFavorites;
    }

    public String getPathOsuJsonPlaySessions() {
        return pathOsuJsonPlaySessions;
    }

    public void setPathOsuJsonPlaySessions(String pathOsuJsonPlaySessions) {
        this.pathOsuJsonPlaySessions = pathOsuJsonPlaySessions;
    }

    public boolean isUpdateCurrentScoreToFile() {
        return updateCurrentScoreToFile;
    }

    public void setUpdateCurrentScoreToFile(boolean updateCurrentScoreToFile) {
        this.updateCurrentScoreToFile = updateCurrentScoreToFile;
    }

    public boolean isForceWriteScoreToFile() {
        return forceWriteScoreToFile;
    }

    public void setForceWriteScoreToFile(boolean forceWriteScoreToFile) {
        this.forceWriteScoreToFile = forceWriteScoreToFile;
    }

    public int getUpdateCurrentScoreFrequencySeconds() {
        return updateCurrentScoreFrequencySeconds;
    }

    public void setUpdateCurrentScoreFrequencySeconds(int updateCurrentScoreFrequencySeconds) {
        this.updateCurrentScoreFrequencySeconds = updateCurrentScoreFrequencySeconds;
    }

    public String getPathCurrentScoreFile() {
        return pathCurrentScoreFile;
    }

    public void setPathCurrentScoreFile(String pathCurrentScoreFile) {
        this.pathCurrentScoreFile = pathCurrentScoreFile;
    }
}
