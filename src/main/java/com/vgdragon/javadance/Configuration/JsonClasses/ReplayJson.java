package com.vgdragon.javadance.Configuration.JsonClasses;


import com.vgdragon.javadance.Configuration.JsonClasses.GlobalClasses.Player;
import com.vgdragon.javadance.Configuration.JsonClasses.ReplayClasses.ReplayData;

import java.util.List;

public class ReplayJson {


    private int version;
    private String gameVersion;
    private String artist;
    private String title;
    private String beatmapVersion;
    private double difficulty;
    private long playedDate;
    private Player playerSelf;
    private List<Player> allPlayers;
    private int gameType;
    private int levelId;
    private int trackId;
    private String beatmapHash;
    private double songLengthSeconds;
    private boolean cancelled;
    private double cancelledTime;
    private String environment;
    private String environmentVersion;
    private int bufferAheadQuarters;
    private double orbSourceDistance;
    private int playerCount;
    private int currentMultiplier;
    private int hitNotesWithNoMiss;
    private int maxHitNotesWithNoMiss;
    private int sessionPauseCount;
    private int currentScore;
    private int previousLeaderboardRank;
    private int newLeaderboardRank;
    private int leaderBoardEntryCount;
    private int hitNotes;
    private int hitOrbs;
    private int hitSliders;
    private int hitSpinners;
    private int missedNotes;
    private int missedOrbs;
    private int missedSliders;
    private int missedSpinners;
    private int hitNotesWithHead;
    private int hitNotesWithLeftHand;
    private int hitNotesWithRightHand;
    private int hitNotesWithLeftFoot;
    private int hitNotesWithRightFoot;
    private int hitNotesWithUnknownHand;
    private int hitNotesForAverage;
    private double cumulativeOffTime;
    private double avgOffTime;
    private double maxOffTime;
    private double minOffTime;
    private double totalDistanceHead;
    private double totalDistanceLeft;
    private double totalDistanceRight;
    private double totalDistanceLeftFoot;
    private double totalDistanceRightFoot;
    private double totalDistanceUnknown;
    private List<ReplayData> replayData;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getGameVersion() {
        return gameVersion;
    }

    public void setGameVersion(String gameVersion) {
        this.gameVersion = gameVersion;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBeatmapVersion() {
        return beatmapVersion;
    }

    public void setBeatmapVersion(String beatmapVersion) {
        this.beatmapVersion = beatmapVersion;
    }

    public double getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(double difficulty) {
        this.difficulty = difficulty;
    }

    public long getPlayedDate() {
        return playedDate;
    }

    public void setPlayedDate(long playedDate) {
        this.playedDate = playedDate;
    }

    public Player getPlayerSelf() {
        return playerSelf;
    }

    public void setPlayerSelf(Player playerSelf) {
        this.playerSelf = playerSelf;
    }

    public List<Player> getAllPlayers() {
        return allPlayers;
    }

    public void setAllPlayers(List<Player> allPlayers) {
        this.allPlayers = allPlayers;
    }

    public int getGameType() {
        return gameType;
    }

    public void setGameType(int gameType) {
        this.gameType = gameType;
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }

    public String getBeatmapHash() {
        return beatmapHash;
    }

    public void setBeatmapHash(String beatmapHash) {
        this.beatmapHash = beatmapHash;
    }

    public double getSongLengthSeconds() {
        return songLengthSeconds;
    }

    public void setSongLengthSeconds(double songLengthSeconds) {
        this.songLengthSeconds = songLengthSeconds;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public double getCancelledTime() {
        return cancelledTime;
    }

    public void setCancelledTime(double cancelledTime) {
        this.cancelledTime = cancelledTime;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getEnvironmentVersion() {
        return environmentVersion;
    }

    public void setEnvironmentVersion(String environmentVersion) {
        this.environmentVersion = environmentVersion;
    }

    public int getBufferAheadQuarters() {
        return bufferAheadQuarters;
    }

    public void setBufferAheadQuarters(int bufferAheadQuarters) {
        this.bufferAheadQuarters = bufferAheadQuarters;
    }

    public double getOrbSourceDistance() {
        return orbSourceDistance;
    }

    public void setOrbSourceDistance(double orbSourceDistance) {
        this.orbSourceDistance = orbSourceDistance;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    public int getCurrentMultiplier() {
        return currentMultiplier;
    }

    public void setCurrentMultiplier(int currentMultiplier) {
        this.currentMultiplier = currentMultiplier;
    }

    public int getHitNotesWithNoMiss() {
        return hitNotesWithNoMiss;
    }

    public void setHitNotesWithNoMiss(int hitNotesWithNoMiss) {
        this.hitNotesWithNoMiss = hitNotesWithNoMiss;
    }

    public int getMaxHitNotesWithNoMiss() {
        return maxHitNotesWithNoMiss;
    }

    public void setMaxHitNotesWithNoMiss(int maxHitNotesWithNoMiss) {
        this.maxHitNotesWithNoMiss = maxHitNotesWithNoMiss;
    }

    public int getSessionPauseCount() {
        return sessionPauseCount;
    }

    public void setSessionPauseCount(int sessionPauseCount) {
        this.sessionPauseCount = sessionPauseCount;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

    public int getPreviousLeaderboardRank() {
        return previousLeaderboardRank;
    }

    public void setPreviousLeaderboardRank(int previousLeaderboardRank) {
        this.previousLeaderboardRank = previousLeaderboardRank;
    }

    public int getNewLeaderboardRank() {
        return newLeaderboardRank;
    }

    public void setNewLeaderboardRank(int newLeaderboardRank) {
        this.newLeaderboardRank = newLeaderboardRank;
    }

    public int getLeaderBoardEntryCount() {
        return leaderBoardEntryCount;
    }

    public void setLeaderBoardEntryCount(int leaderBoardEntryCount) {
        this.leaderBoardEntryCount = leaderBoardEntryCount;
    }

    public int getHitNotes() {
        return hitNotes;
    }

    public void setHitNotes(int hitNotes) {
        this.hitNotes = hitNotes;
    }

    public int getHitOrbs() {
        return hitOrbs;
    }

    public void setHitOrbs(int hitOrbs) {
        this.hitOrbs = hitOrbs;
    }

    public int getHitSliders() {
        return hitSliders;
    }

    public void setHitSliders(int hitSliders) {
        this.hitSliders = hitSliders;
    }

    public int getHitSpinners() {
        return hitSpinners;
    }

    public void setHitSpinners(int hitSpinners) {
        this.hitSpinners = hitSpinners;
    }

    public int getMissedNotes() {
        return missedNotes;
    }

    public void setMissedNotes(int missedNotes) {
        this.missedNotes = missedNotes;
    }

    public int getMissedOrbs() {
        return missedOrbs;
    }

    public void setMissedOrbs(int missedOrbs) {
        this.missedOrbs = missedOrbs;
    }

    public int getMissedSliders() {
        return missedSliders;
    }

    public void setMissedSliders(int missedSliders) {
        this.missedSliders = missedSliders;
    }

    public int getMissedSpinners() {
        return missedSpinners;
    }

    public void setMissedSpinners(int missedSpinners) {
        this.missedSpinners = missedSpinners;
    }

    public int getHitNotesWithHead() {
        return hitNotesWithHead;
    }

    public void setHitNotesWithHead(int hitNotesWithHead) {
        this.hitNotesWithHead = hitNotesWithHead;
    }

    public int getHitNotesWithLeftHand() {
        return hitNotesWithLeftHand;
    }

    public void setHitNotesWithLeftHand(int hitNotesWithLeftHand) {
        this.hitNotesWithLeftHand = hitNotesWithLeftHand;
    }

    public int getHitNotesWithRightHand() {
        return hitNotesWithRightHand;
    }

    public void setHitNotesWithRightHand(int hitNotesWithRightHand) {
        this.hitNotesWithRightHand = hitNotesWithRightHand;
    }

    public int getHitNotesWithLeftFoot() {
        return hitNotesWithLeftFoot;
    }

    public void setHitNotesWithLeftFoot(int hitNotesWithLeftFoot) {
        this.hitNotesWithLeftFoot = hitNotesWithLeftFoot;
    }

    public int getHitNotesWithRightFoot() {
        return hitNotesWithRightFoot;
    }

    public void setHitNotesWithRightFoot(int hitNotesWithRightFoot) {
        this.hitNotesWithRightFoot = hitNotesWithRightFoot;
    }

    public int getHitNotesWithUnknownHand() {
        return hitNotesWithUnknownHand;
    }

    public void setHitNotesWithUnknownHand(int hitNotesWithUnknownHand) {
        this.hitNotesWithUnknownHand = hitNotesWithUnknownHand;
    }

    public int getHitNotesForAverage() {
        return hitNotesForAverage;
    }

    public void setHitNotesForAverage(int hitNotesForAverage) {
        this.hitNotesForAverage = hitNotesForAverage;
    }

    public double getCumulativeOffTime() {
        return cumulativeOffTime;
    }

    public void setCumulativeOffTime(double cumulativeOffTime) {
        this.cumulativeOffTime = cumulativeOffTime;
    }

    public double getAvgOffTime() {
        return avgOffTime;
    }

    public void setAvgOffTime(double avgOffTime) {
        this.avgOffTime = avgOffTime;
    }

    public double getMaxOffTime() {
        return maxOffTime;
    }

    public void setMaxOffTime(double maxOffTime) {
        this.maxOffTime = maxOffTime;
    }

    public double getMinOffTime() {
        return minOffTime;
    }

    public void setMinOffTime(double minOffTime) {
        this.minOffTime = minOffTime;
    }

    public double getTotalDistanceHead() {
        return totalDistanceHead;
    }

    public void setTotalDistanceHead(double totalDistanceHead) {
        this.totalDistanceHead = totalDistanceHead;
    }

    public double getTotalDistanceLeft() {
        return totalDistanceLeft;
    }

    public void setTotalDistanceLeft(double totalDistanceLeft) {
        this.totalDistanceLeft = totalDistanceLeft;
    }

    public double getTotalDistanceRight() {
        return totalDistanceRight;
    }

    public void setTotalDistanceRight(double totalDistanceRight) {
        this.totalDistanceRight = totalDistanceRight;
    }

    public double getTotalDistanceLeftFoot() {
        return totalDistanceLeftFoot;
    }

    public void setTotalDistanceLeftFoot(double totalDistanceLeftFoot) {
        this.totalDistanceLeftFoot = totalDistanceLeftFoot;
    }

    public double getTotalDistanceRightFoot() {
        return totalDistanceRightFoot;
    }

    public void setTotalDistanceRightFoot(double totalDistanceRightFoot) {
        this.totalDistanceRightFoot = totalDistanceRightFoot;
    }

    public double getTotalDistanceUnknown() {
        return totalDistanceUnknown;
    }

    public void setTotalDistanceUnknown(double totalDistanceUnknown) {
        this.totalDistanceUnknown = totalDistanceUnknown;
    }

    public List<ReplayData> getReplayData() {
        return replayData;
    }

    public void setReplayData(List<ReplayData> replayData) {
        this.replayData = replayData;
    }
}
