package com.vgdragon.javadance.Configuration.JsonClasses;

import com.vgdragon.javadance.Configuration.JsonClasses.GlobalClasses.PlaySession;

import java.util.List;

public class PlaySessionsJson {

    private int version;
    private List<PlaySession> playSessions;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public List<PlaySession> getPlaySessions() {
        return playSessions;
    }

    public void setPlaySessions(List<PlaySession> playSessions) {
        this.playSessions = playSessions;
    }
}
